#include <stdio.h>
#include <string.h>
#include "md5.h"
#include <stdlib.h>

int main(int countWord, char *fileArray[])
{
    
    
    FILE *read = fopen(fileArray[1], "r");
    if(!read)
    {
        printf("Can't open %s for reading\n", fileArray[1]);
        exit(1);
    }
    
    FILE *dest = fopen(fileArray[2], "w");
    if(!dest)
    {
        printf("Can't open %s for writing\n", fileArray[2]);
        exit(1);
    }
    
    char words[1000];
    
    while( fgets(words, 1000, read) != NULL)
    {
        words[strlen(words)-1] = '\0';
        char *hash = md5(words, strlen(words));
        fprintf(dest, "%s\n", hash);
        free(hash);
    }
}
